/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const App = () => {
  const getDataUsingGet = () => {
    fetch('https://react.servertestdom.xyz/item.php?item_id=1', {
      method: 'GET',
    })
      .then((response) => response.json())
      .then((responseJson) => {
        alert(JSON.stringify(responseJson));
        console.log(responseJson);
      })
      .catch((error) => {
        alert(JSON.stringify(error));
        console.log(error);
      });
  };

  const getDataUsingPost = () => {
    const dataToSend = {item_id: 1};
    var formBody = [];
    for (var key in dataToSend) {
      var encodedKey = encodeURIComponent(key);
      var encodedValue = encodeURIComponent(dataToSend[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    fetch('https://react.servertestdom.xyz/item.php', {
      method: 'POST',
      body: formBody,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        alert(JSON.stringify(responseJson));
        console.log(responseJson);
      })
      .catch((error) => {
        alert(JSON.stringify(error));
        console.log(error);
      });
  };

  const sendPut = () => {
    fetch('https://react.servertestdom.xyz/text.txt', {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'text/plain',
      },
      body: 'TEST',
    })
      //.then((response) => response.json())
      .then((resp) => {
        alert('Put status: ' + (resp.status === 200 ? 'OK' : 'ERROR') + '[' + resp.status + ']');
      })
      .catch((error) => {
        alert(error);
      });
  };

  const sendDelete = () => {
    fetch('https://react.servertestdom.xyz/text.txt', {
      method: 'DELETE',
    }).then((resp) =>
      alert('Delete status: ' + (resp.status === 200 ? 'OK' : 'ERROR') + '[' + resp.status + ']'),
    );
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        <View style={styles.container}>
          {/*Running GET Request*/}
          <TouchableOpacity
            style={styles.buttonStyle}
            onPress={getDataUsingGet}>
            <Text style={styles.textStyle}>Get Data Using GET</Text>
          </TouchableOpacity>
          {/*Running POST Request*/}
          <TouchableOpacity
            style={styles.buttonStyle}
            onPress={getDataUsingPost}>
            <Text style={styles.textStyle}>Get Data Using POST</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.buttonStyle}
            onPress={sendPut}>
            <Text style={styles.textStyle}>Send PUT Request</Text>
          </TouchableOpacity>
          <TouchableOpacity
              style={styles.buttonStyle}
              onPress={sendDelete}>
            <Text style={styles.textStyle}>Send DELETE Request</Text>
          </TouchableOpacity>
        </View>
        <Text
          style={{
            fontSize: 18,
            textAlign: 'center',
            color: 'grey',
          }}
        />
        <Text
          style={{
            fontSize: 16,
            textAlign: 'center',
            color: 'grey',
          }}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    padding: 20,
  },
  textStyle: {
    fontSize: 18,
    color: 'white',
  },
  buttonStyle: {
    alignItems: 'center',
    backgroundColor: '#f4511e',
    padding: 10,
    marginVertical: 10,
  },
});

export default App;
